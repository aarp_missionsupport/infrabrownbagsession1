# Welcome to the AWS Infrastructure Team Brown Bag

## Cloud Formation and Infrastructure Automation
1. Cloudformation is a Tool that enables provisioning of resources.
2. Involves understanding JSON or YAML syntax
3. Help Establish Controls on Resources provisioned
4. Make it repeatable
5. Enable Configuration Management

## References:

1. https://docs.aws.amazon.com/cli/latest/reference/cloudformation/index.html
2. https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ec2-instance.html#cfn-ec2-instance-instancetype 
3. Get Attributes - https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-getatt.html
4. AWS Parameters - https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/parameters-section-structure.html#aws-specific-parameter-types

## Walkthroughs

1.    EC2 Creation via CloudFormation Template

The structure of the file comprises of Resources and Outputs. Resources are the items that are provisioned and the Outputs are details about the provisioned EC2 Instances.

```
aws cloudformation create-stack --template-body file://01CF_EC2_Provisioning.yml --stack-name CFEC2Instance01
```

Navigate to the Cloud Formation Console and view the Instance Public DNS in Outputs. 

Navigate to the EC2 Console and validate the existence of the Machine

```
aws cloudformation delete-stack --stack-name CFEC2Instance01
```

Navigate to EC2 and point the Resources in the Stack being destroyed.

2.    EC2 Creation Limit the Instance Types:
Controlling the type of Instances a User can provision by Parameters Value.
Notice the Parameters value.

Navigate to the Console and Upload the Cloud Formation Template.

```
aws cloudformation create-stack --template-body file://02CF_EC2_ProvisioningWithParameter.yml --stack-name CFEC2Instance02 --parameters ParameterKey=InstanceTypeParam,ParameterValue=t2.nano
```

Note the Parameter Value is being passed from the CLI.

```
aws cloudformation delete-stack --stack-name CFEC2Instance02
```

Now lets pass a Instance Type which is not one of the accepted values

```
aws cloudformation create-stack --template-body file://02CF_EC2_ProvisioningWithParameter.yml --stack-name CFEC2Instance03 --parameters ParameterKey=InstanceTypeParam,ParameterValue=t2.large
```

Result of the Command - An error occurred (ValidationError) when calling the CreateStack operation: Parameter 'InstanceTypeParam' must be one of AllowedValues

3.    EC2 Creation with Other Resources:
In the earlier examples we created the Instance with Security Group that was already provisioned. But not always do we know what the Instance Security Group i.e. what should be the ingress and egress. 
Here we are building a Web Server and want 80 to the world and 22 to my IP Address. Note the VPCID specified is the one that is already provisioned. 

```
aws cloudformation create-stack --template-body file://03CF_EC2_ProvisioningMoreThanOneResource.yml --stack-name CFEC2Instance03
```

Navigate to the console and display the EC2 Instance created.

```
aws cloudformation delete-stack --stack-name CFEC2Instance03
```

4.    EC2 Creation with AWS Parameters:
In 1 and 3 we did see the need for values specific to the Account; Security Groups and VPC. AWS Provides a list of Parameters [4]. Here we are planning to use the parameter. 

Demonstrate in the Cloud Formation Console. 

We could use the aws cli query, to determine the vpc id we are interested in. Here I am just picking the first one
```
aws ec2 describe-vpcs --output json --query Vpcs[0].VpcId
```

5.    EC2 Creation with Apache Installed and Web Page Provisioned aka UserData

Another interesting aspect of Server Provisioning is the User Data attribute, which lets you run scripts when the Server is provisioned. 
Note here we have now provisioned a Web Server with HTTPD service and configured it to restart at every reboot.

```
aws cloudformation delete-stack --stack-name CFEC2Instance05
```

6.  Jenkins Server
Stitching what we learned today, here is an involved template that I borrowed and modified to implement a Jenkins Server. I am using a Bitnami AMI - Marketplace AMI to provision a machine instead of using the UserData scripts to install Jenkins.

```
aws cloudformation create-stack --template-body file://06CF_EC2_Jenkins.yml --stack-name CFEC2Instance06 --parameters ParameterKey=KeyPairParam,ParameterValue=aarpbitbucketpem
```

Note:
    1. Public Subnet Created
    2. Internet Gateway Attached
    3. Ports opened are 22 and 80 - 8080 
    4. Jenkins is provisioned with a default password (last time I did it)

Delete the Stack
```
aws cloudformation delete-stack --stack-name CFEC2Instance06
```

7.  Ensuring only certain users in the group have access to provision the EC2Instances.

```
aws cloudformation create-stack --template-body file://01CF_EC2_Provisioning.yml --stack-name CFEC2Instance07 --profile manager
```

8.  Notifications

One of the monitoring controls that we need to be aware in the Cloud is the number of resources being provisioned across the environments, I wanted to demonstrate the CloudWatch Logs and setting up Events and Alerts.

Cloud Watch Events and monitoring in place for new instance creation and instance termination is being configured for truncation. The subcription can be made to send an email or call an HTTP method.

